const typeOrmConfig: any = {
  type: process.env.DB_TYPE || 'postgres',
  host: process.env.DB_HOST || 'localhost',
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USER || 'postgres',
  password: process.env.DB_SECRET || 'postgres',
  database: process.env.DB_NAME || 'todos',
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: process.env.DB_SYNC || true,
  ssl: process.env.DB_SSL ? {rejectUnauthorized: false} : Boolean(process.env.DB_SSL)
};

export default typeOrmConfig;
