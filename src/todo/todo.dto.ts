export class CreateTodoDto{
  name : string ;
  dateCompletion : Date;
  completed : boolean
}