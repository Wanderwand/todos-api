import { Body, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { create } from 'domain';
import { threadId } from 'worker_threads';
import { Todo } from './entities/todo.entity';
import { CreateTodoDto } from './todo.dto';
import { TodoRepository } from './todo.repository';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(TodoRepository)
    private todoRepository : TodoRepository,
  ){}


  async getAllTodos(): Promise<Todo[]> {
    return await this.todoRepository.find()
  }

  async createTodo(createTodoDto : CreateTodoDto){
    console.log(createTodoDto)
    const {name , dateCompletion ,completed } = createTodoDto ;
    
    const todo = new Todo();

    todo.name = name ;
    todo.dateCompletion = dateCompletion;
    todo.completed = completed ;
    await this.todoRepository.save(todo);
    return todo
  }

  async updateStausCompletion(id: number , status : boolean ):Promise<Todo[]>{
    const todo = await this.todoRepository.findOne(id)
    todo.completed  = status;
    await this.todoRepository.save(todo)
    return this.getAllTodos()
  }

  async deleteTodo(id:number){
    console.log(id)

    if(this.todoRepository.findOne(id)){
      await this.todoRepository.delete(id)
    }
}
  async reset(){
    return this.todoRepository.clear()
  }

}
