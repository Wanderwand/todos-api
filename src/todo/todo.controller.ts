import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { Todo } from './entities/todo.entity';
import { CreateTodoDto } from './todo.dto';
import { TodoService } from './todo.service';

@Controller('todo')
export class TodoController {
  constructor(private todoService: TodoService){}
  
  @Get()
  getTodo(): Promise<Todo[]>{
    return this.todoService.getAllTodos()
  } 

  @Post()
  createTodo(@Body() createTodoDto : CreateTodoDto) :any {
     return  this.todoService.createTodo(createTodoDto)
  }

  @Get('/reset')
  reset(){
    return this.todoService.reset()
  }

  @Put()
  updateStausCompletion(
    @Body('id') id:number,
    @Body('status') status : boolean 
  ):Promise<Todo[]>{
    return this.todoService.updateStausCompletion(id,status)

  }

  @Delete('/:id')
  deleteTodo(@Param('id') id:number){  
    return this.todoService.deleteTodo(id)
  }
}
