import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Todo {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
    
    @Column()
    dateCompletion: Date ;

    @Column()
    completed : boolean ; 

    @CreateDateColumn()
    dateCreated: Date;

    @UpdateDateColumn()
    dateUpdated : Date;

}